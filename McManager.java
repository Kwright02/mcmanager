package com.Kwright02.McManager;

import java.net.InetSocketAddress;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

public class McManager extends JavaPlugin {

	@Override
	public void onEnable(){
    getConfig().options().copyDefaults();
    saveConfig();
	}
	@Override
	public void onDisable(){
	
	}
	
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
	if(cmd.getName().equalsIgnoreCase("ban")){
	if(sender instanceof Player){
	if(sender.hasPermission("mc.ban")){
	Player target = Bukkit.getPlayerExact(args[0]);
	target.setBanned(true);
	target.kickPlayer(ChatColor.translateAlternateColorCodes('&', this.getConfig().getString("BanMessage1")) + "" + args[1] + "" + ChatColor.translateAlternateColorCodes('&', this.getConfig().getString("BanMessage2")));
	if(cmd.getName().equalsIgnoreCase("mcreport")){
	if(sender.hasPermission("mc.mcreport")){
	Player target1 = Bukkit.getPlayerExact(args[0]);
	int ping = ((CraftPlayer) target1).getHandle().ping;
	InetSocketAddress IpTarget = getServer().getPlayer(args[0]).getAddress();
	sender.sendMessage(ChatColor.GOLD + "This Is The McReport For Player: " + ChatColor.RED + target1);
	sender.sendMessage(ChatColor.GREEN + "Player's Ping, Ip, First Join Date And Last Played:" + IpTarget + ping + target1.getFirstPlayed() + target1.getLastPlayed());
	sender.sendMessage(ChatColor.AQUA + "Player's Display Name:" + target1.getDisplayName());
	sender.sendMessage(ChatColor.BLUE + "Misc Things" + "Exp:" + target1.getExp() + "Health" + target1.getHealth() + "Food Level:" + target1.getFoodLevel() + "Active Potion Effects:" + target1.getActivePotionEffects() + "Can They Fly? " + target1.getAllowFlight() + "Location:" + target1.getLocation());
	}
	}
	
	
	} else { 
		
	sender.sendMessage("§4No Permission, or Incorrect Usage Of Command");

	}
	}
	}
	return false;
	}
	
	 
}

